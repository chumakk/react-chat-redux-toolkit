import Chat from "./src/Components/Chat";
import rootReducer from "./src/store/chat/reducer";

export default {
  Chat,
  rootReducer,
};
