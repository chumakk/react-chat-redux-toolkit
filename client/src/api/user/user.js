class User {
  constructor(http) {
    this._http = http;
  }
  getUsers() {
    return this._http.load("/api/users");
  }

  getUserById(id) {
    return this._http.load(`/api/users/${id}`);
  }

  deleteUser(id) {
    return this._http.load("/api/users", {
      method: "DELETE",
      contentType: "application/json",
      payload: JSON.stringify({ id }),
    });
  }

  createUser(data) {
    return this._http.load("/api/users", {
      method: "POST",
      contentType: "application/json",
      payload: JSON.stringify(data),
    });
  }

  updateUser(data) {
    return this._http.load("/api/users/update", {
      method: "PUT",
      contentType: "application/json",
      payload: JSON.stringify(data),
    });
  }
}

export default User;
