import HttpError from "../../common/http-error";

class Http {
  load(url, options = {}) {
    const { method = "GET", payload = null, contentType } = options;
    return fetch(url, {
      method,
      headers: {
        "Content-Type": contentType,
      },
      body: payload,
    })
      .then(this._checkStatus)
      .then((res) => res.json())
      .catch((e) => {
        throw e;
      });
  }

  async _checkStatus(response) {
    if (!response.ok) {
      const parsedException = await response.json().catch(() => ({
        message: response.statusText,
      }));

      throw new HttpError({
        code: `${response.status}`,
        message: parsedException?.message,
      });
    }
    return response;
  }
}

export default Http;
