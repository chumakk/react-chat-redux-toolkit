class Auth {
  constructor(http) {
    this._http = http;
  }
  login(data) {
    return this._http.load("/api/auth", {
      method: "POST",
      contentType: "application/json",
      payload: JSON.stringify(data),
    });
  }
}

export default Auth;
