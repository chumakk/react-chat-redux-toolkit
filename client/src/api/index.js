import Auth from "./auth/auth";
import Http from "./http/http";
import User from "./user/user";
import Message from "./message/message";

const http = new Http();

const authService = new Auth(http);

const userService = new User(http);

const messageService = new Message(http);

export { authService, userService, messageService };
