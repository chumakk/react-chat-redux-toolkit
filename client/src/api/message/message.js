class Message {
  constructor(http) {
    this._http = http;
  }

  getMessages() {
    return this._http.load("/api/messages");
  }

  createMessage(data) {
    return this._http.load("api/messages", {
      method: "POST",
      contentType: "application/json",
      payload: JSON.stringify(data),
    });
  }

  updateMessage(data) {
    return this._http.load("api/messages/update", {
      method: "PUT",
      contentType: "application/json",
      payload: JSON.stringify(data),
    });
  }

  toggleLikeMessage(data) {
    return this._http.load("api/messages/react", {
      method: "POST",
      contentType: "application/json",
      payload: JSON.stringify(data),
    });
  }

  deleteMessage(id) {
    return this._http.load("api/messages", {
      method: "DELETE",
      contentType: "application/json",
      payload: JSON.stringify({ id }),
    });
  }
}

export default Message;
