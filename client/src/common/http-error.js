class HttpError extends Error {
  constructor({ code = 500, message }) {
    super(message);
    this.code = code;
  }
}

export default HttpError;
