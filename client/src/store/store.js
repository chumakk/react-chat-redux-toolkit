import { configureStore } from "@reduxjs/toolkit";
import chatReducer from "./chat/reducer";
import userReducer from "./user/reducer";
import adminReducer from "./admin/reducer";

const store = configureStore({
  reducer: { chat: chatReducer, user: userReducer, admin: adminReducer },
});

export default store;
