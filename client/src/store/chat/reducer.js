import { createReducer } from "@reduxjs/toolkit";
import * as actions from "./actions";

const initialState = {
  messages: [],
  editMessage: null,
  canScroll: true,
};

const chatReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(actions.fetchMessages.fulfilled, (state, action) => {
      state.messages = action.payload;
    })
    .addCase(actions.setMessages, (state, action) => {
      state.messages = action.payload;
    })
    .addCase(actions.addMessage.fulfilled, (state, action) => {
      state.messages = [...state.messages, action.payload];
      state.canScroll = true;
    })
    .addCase(actions.updateMessage.fulfilled, (state, action) => {
      state.editMessage = action.payload;
    })
    .addCase(actions.deleteMessage.fulfilled, (state, action) => {
      state.messages = state.messages.filter(
        (mes) => mes.id !== action.payload[0].id
      );
    })
    .addCase(actions.toggleLikeMessage.fulfilled, (state, action) => {
      const { id } = action.payload;
      state.messages = state.messages.map((mes) =>
        mes.id !== id ? mes : action.payload
      );
    })
    .addCase(actions.setEditMessage, (state, action) => {
      const message = state.messages.find((mes) => mes.id === action.payload);
      state.editMessage = message;
    })
    .addCase(actions.unsetEditMessage, (state) => {
      state.editMessage = null;
    })
    .addCase(actions.setCanScroll, (state, action) => {
      state.canScroll = action.payload;
    })
});

export default chatReducer;
