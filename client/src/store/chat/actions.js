import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { messageService } from "../../api";

const Actions = {
  fetchMessages: "chat/fetch-messages",
  setMessages: "chat/set-messages",
  addMessage: "chat/add-message",
  updateMessage: "chat/update-message",
  deleteMessage: "chat/delete-message",
  toggleLikeMessage: "chat/like-message",
  setEditMessage: "chat/set-edit-message",
  unsetEditMessage: "chat/unset-edit-message",
  setCanScroll: "chat/set-can-scroll",
};

const setMessages = createAction(Actions.setMessages)

const fetchMessages = createAsyncThunk(Actions.fetchMessages, async () => {
  const messages = await messageService.getMessages();
  return messages;
});

const addMessage = createAsyncThunk(Actions.addMessage, async (data) => {
  const message = await messageService.createMessage(data);
  return message;
});

const setEditMessage = createAction(Actions.setEditMessage);

const deleteMessage = createAsyncThunk(Actions.deleteMessage, async (id) => {
  const message = await messageService.deleteMessage(id);
  return message;
});

const toggleLikeMessage = createAsyncThunk(
  Actions.toggleLikeMessage,
  async (data) => {
    const message = await messageService.toggleLikeMessage(data);
    return message;
  }
);

const setCanScroll = createAction(Actions.setCanScroll);

const updateMessage = createAsyncThunk(Actions.updateMessage, async (mes) => {
  return messageService.updateMessage(mes);
});

const unsetEditMessage = createAction(Actions.unsetEditMessage);

export {
  fetchMessages,
  setMessages,
  addMessage,
  setEditMessage,
  updateMessage,
  deleteMessage,
  toggleLikeMessage,
  setCanScroll,
  unsetEditMessage
};
