import { authService } from "../../api";
import { createAsyncThunk, createAction } from "@reduxjs/toolkit";

const Actions = {
  login: "user/login",
  logOut: "user/logOut",
};

const logOut = createAction(Actions.logOut);

const login = createAsyncThunk(Actions.login, async (data) => {
  const user = await authService.login(data);
  return user;
});

export { login, logOut };
