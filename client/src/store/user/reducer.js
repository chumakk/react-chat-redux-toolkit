import { createReducer } from "@reduxjs/toolkit";
import * as actions from "./actions";

const initialState = {
  user: null,
};

const userReducer = createReducer(initialState, (builder) => {
  builder.addCase(actions.login.fulfilled, (state, action) => {
    state.user = action.payload;
  });
  builder.addCase(actions.logOut, (state) => {
    state.user = null;
  });
});

export default userReducer;
