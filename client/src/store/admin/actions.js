import { userService } from "../../api";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";

const Actions = {
  getUsers: "admin/get-users",
  createUser: "admin/create-user",
  deleteUser: "admin/delete-user",
  setUserToUpdate: "admin/set-user-to-update",
  updateUser: "admin/update-user",
  setUserList: "admin/set-user-list",
  setEditUser: "admin/set-edit-user",
  nullify: "admin/nullify",
};

const setUserList = createAction(Actions.setUserList);

const unsetEditUser = createAction(Actions.setEditUser);

const getUsers = createAsyncThunk(Actions.getUsers, async () => {
  const users = await userService.getUsers();
  return users;
});

const createUser = createAsyncThunk(
  Actions.createUser,
  async (data) => {
    const user = await userService.createUser(data);
    return user;
  }
);

const deleteUser = createAsyncThunk(
  Actions.deleteUser,
  async (id) => {
    const user = await userService.deleteUser(id);
    return user;
  }
);

const setUserToUpdate = createAsyncThunk(
  Actions.setUserToUpdate,
  async (id) => {
    const user = await userService.getUserById(id);
    return user;
  }
);

const updateUser = createAsyncThunk(
  Actions.setUserToUpdate,
  async (u) => {
    const user = await userService.updateUser(u);
    return user;
  }
);

export {
  setUserList,
  getUsers,
  deleteUser,
  setUserToUpdate,
  createUser,
  updateUser,
  unsetEditUser,
};
