import { createReducer } from "@reduxjs/toolkit";
import * as actions from "./actions";

const initialState = {
  usersList: [],
  editUser: null,
};

const userReducer = createReducer(initialState, (builder) => {
  builder.addCase(actions.setUserList, (state, action) => {
    state.usersList = action.payload;
  });

  builder.addCase(actions.unsetEditUser, (state) => {
    state.editUser = null;
  });

  builder.addCase(actions.getUsers.fulfilled, (state, action) => {
    state.usersList = action.payload;
  });

  builder.addCase(actions.deleteUser.fulfilled, (state, action) => {
    state.usersList = state.usersList.filter(
      (user) => user.id !== action.payload[0].id
    );
  });

  builder.addCase(actions.setUserToUpdate.fulfilled, (state, action) => {
    state.editUser = action.payload;
  });
});

export default userReducer;
