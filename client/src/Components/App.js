import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Redirect,
  Switch,
  Route,
  useLocation,
  NavLink,
} from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import 'react-notifications/lib/notifications.css';
import Login from "./Login/Login";
import UserList from "./Admin/UserList/UserList";
import UserEditor from "./Admin/UserEditor/UserEditor";
import { logOut } from "../store/user/actions";
import Chat from "./Chat/Chat";
import MessageEditor from "./Chat/MessageEditor/MessageEditor";
import { unsetEditMessage } from "../store/chat/actions";
import { unsetEditUser } from "../store/admin/actions";

import "./App.css";

const App = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);
  const location = useLocation();

  const handleCleanMessageEditor = useCallback(() => {
    dispatch(unsetEditMessage());
  }, [dispatch]);

  const handleCleanUserEditor = useCallback(() => {
    dispatch(unsetEditUser());
  }, [dispatch]);

  if (!user && location.pathname !== "/login") return <Redirect to="/login" />;

  if (user && location.pathname === "/login") {
    return user.role === "admin" ? (
      <Redirect to="/userlist" />
    ) : (
      <Redirect to="/chat" />
    );
  }

  return (
    <div className="App">
      {user && (
        <div className="app-header">
          <NavLink
            onClick={handleCleanMessageEditor}
            className="link"
            to="/chat"
          >
            Chat
          </NavLink>
          {user.role === "admin" && (
            <NavLink
              className="link"
              to="/userlist"
              onClick={handleCleanUserEditor}
            >
              User list
            </NavLink>
          )}
          <button className="link logout" onClick={() => dispatch(logOut())}>
            Log out
          </button>
        </div>
      )}
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/userlist" component={UserList} />
        <Route path="/usereditor" component={UserEditor} />
        <Route path="/chat" component={Chat} />
        <Route path="/message-editor" component={MessageEditor} />
      </Switch>
      <NotificationContainer />
    </div>
  );
};

export default App;
