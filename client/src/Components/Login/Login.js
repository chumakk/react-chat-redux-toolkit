import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { login } from "../../store/user/actions";
import "./Login.css";
import { NotificationManager } from "react-notifications";
import Preloader from "../Preloaders/middlePreloader/Preloader";

const Login = () => {
  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [isFetch, setIsFetch] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsFetch(true);
    try {
      await dispatch(login({ user: name, password })).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
      setIsFetch(false);
    }
  };

  return (
    <div className="login-wrapper">
      <div className="form-container">
        <div className="login-title">Login</div>
        <form onSubmit={handleSubmit} className="login-form">
          <div className="input-container">
            <label className="input-label">Name</label>
            <input
              className="input-el"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="input-container">
            <label className="input-label">Password</label>
            <input
              className="input-el"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <button className="login-submit">Login</button>
          </div>
        </form>
        {isFetch && <Preloader />}
      </div>
    </div>
  );
};

export default Login;
