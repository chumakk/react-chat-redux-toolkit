import React, { useCallback, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  getUsers,
  deleteUser,
  setUserToUpdate,
  setUserList,
} from "../../../store/admin/actions";
import { Link, Redirect } from "react-router-dom";
import "./UserList.css";
import User from "./User/User";
import { NotificationManager } from "react-notifications";
import Preloader from "../../Preloaders/middlePreloader/Preloader";

const UserList = () => {
  const dispatch = useDispatch();
  const { usersList, editUser } = useSelector((state) => ({
    editUser: state.admin.editUser,
    usersList: state.admin.usersList,
  }));

  const [isFetch, setIsFetch] = useState(false);
  const [isFetchUser, setIsFetchUser] = useState(false);

  const fetchUsers = useCallback(async () => {
    try {
      setIsFetch(true);
      await dispatch(getUsers()).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  }, [dispatch, setIsFetch]);

  useEffect(() => {
    fetchUsers();
    return () => dispatch(setUserList([]));
  }, [dispatch, fetchUsers]);

  const handleDeleteUser = async (id) => {
    setIsFetchUser(true);
    try {
      await dispatch(deleteUser(id)).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetchUser(false);
    }
  };

  const handleSetUserToUpdate = async (id) => {
    setIsFetchUser(true);
    try {
      await dispatch(setUserToUpdate(id)).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetchUser(false);
    }
  };

  if (editUser) {
    return <Redirect to="/usereditor" />;
  }

  return (
    <div className="users-list">
      <div className="users-list-title">User list</div>
      <div className="add-user-wrapper">
        <Link className="admin-add-user" to="/usereditor">
          Add user
        </Link>
      </div>
      <div className="users-container">
        {usersList.map((user) => (
          <User
            key={user.id}
            userOnUpdate={handleSetUserToUpdate}
            deleteUser={handleDeleteUser}
            user={user.user}
            id={user.id}
            isFetch={isFetchUser}
          />
        ))}
        {isFetch && <Preloader />}
      </div>
    </div>
  );
};

export default UserList;
