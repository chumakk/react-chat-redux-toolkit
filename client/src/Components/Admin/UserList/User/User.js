import React from "react";
import "./User.css";

const User = (props) => {
  const { deleteUser, userOnUpdate, id, user, isFetch } = props;

  return (
    <div className="user-wrapper">
      <div className="user-name">{user}</div>
      <div className="button-container">
        <button
          disabled={isFetch}
          className="admin-action-btn admin-action-edit"
          onClick={() => userOnUpdate(id)}
        >
          Edit
        </button>
        <button
          disabled={isFetch}
          className="admin-action-btn"
          onClick={() => deleteUser(id)}
        >
          Delete
        </button>
      </div>
    </div>
  );
};

export default User;
