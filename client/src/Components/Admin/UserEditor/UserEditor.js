import React, { useCallback, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  createUser,
  updateUser,
  unsetEditUser,
} from "../../../store/admin/actions";
import "./UserEditor.css";
import { NotificationManager } from "react-notifications";
import Preloader from "../../Preloaders/middlePreloader/Preloader";
import { Link } from "react-router-dom";

const UserEditor = () => {
  const editUser = useSelector((state) => state.admin.editUser);
  const dispatch = useDispatch();

  const [name, setName] = useState(editUser?.user ?? "");
  const [password, setPassword] = useState(editUser?.password ?? "");
  const [avatar, setAvatar] = useState(editUser?.avatar ?? "");
  const [role, setRole] = useState(editUser?.role ?? "user");

  const [isFetch, setIsFetch] = useState(false);

  const handleUnsetEditUser = useCallback(() => {
    if (editUser) {
      dispatch(unsetEditUser());
    }
  }, [dispatch, editUser]);

  useEffect(() => {
    return () => {
      handleUnsetEditUser();
    };
  }, [handleUnsetEditUser]);

  const validFields = () => {
    if (name.length === 0 || password.length === 0) {
      NotificationManager.error("name and password mustn't be empty");
      return false
    }
    return true;
  };

  const handleAddSubmit = async (e) => {
    e.preventDefault();
    if (!validFields()) return;
    setIsFetch(true);
    const user = {
      user: name,
      password,
      avatar,
      role,
    };
    try {
      await dispatch(createUser(user)).unwrap();
      setName("");
      setPassword("");
      setAvatar("");
      setRole("user");
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  };

  const handleSaveSubmit = async (e) => {
    e.preventDefault();
    if (!validFields()) return;
    setIsFetch(true);
    const user = {
      id: editUser.id,
      user: name,
      password,
      avatar,
      role,
    };
    try {
      await dispatch(updateUser(user)).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  };

  return (
    <div className="user-editor-wrapper">
      <div className="user-editor-title">
        {editUser ? "Edit user" : "Create user"}
      </div>
      <form
        onSubmit={editUser ? handleSaveSubmit : handleAddSubmit}
        className="user-editor"
      >
        <div className="editor-input-container">
          <label className="editor-input-label">Name</label>
          <input
            value={name}
            onChange={(e) => setName(e.target.value)}
            className="editor-input-el"
          />
        </div>
        <div className="editor-input-container">
          <label className="editor-input-label">Password</label>
          <input
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className="editor-input-el"
          />
        </div>
        <div className="editor-input-container">
          <label className="editor-input-label">Avatar(url)</label>
          <input
            value={avatar}
            onChange={(e) => setAvatar(e.target.value)}
            className="editor-input-el"
          />
        </div>
        <div className="editor-input-container">
          <label className="editor-input-label">Role</label>
          <select
            value={role}
            required
            onChange={(e) => setRole(e.target.value)}
          >
            <option value="user">User</option>
            <option value="admin">Admin</option>
          </select>
        </div>
        <div className="admin-edit-actions-wrapper">
          <button className="admin-save-button">
            {editUser ? "Save" : "Add"}
          </button>
          <Link
            className="admin-cancel-button"
            to="/userlist"
            onClick={handleUnsetEditUser}
          >
            Cancel
          </Link>
        </div>
      </form>
      {isFetch && <Preloader />}
    </div>
  );
};

export default UserEditor;
