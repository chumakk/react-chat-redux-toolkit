import React, { useCallback, useEffect, useRef, useState } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { NotificationManager } from "react-notifications";
import {
  setCanScroll,
  setEditMessage,
  deleteMessage,
  toggleLikeMessage,
  setMessages
} from "../../../store/chat/actions";
import "./MessageList.css";
import Message from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";
import moment from "moment";
import { Redirect } from "react-router-dom";

let timeOfLastDivider;
const MessageList = () => {
  const bottom = useRef(null);

  const { canScroll, ownerId, messages, editMessage } = useSelector(
    (state) => ({
      canScroll: state.chat.canScroll,
      ownerId: state.user.user.id,
      messages: state.chat.messages,
      editMessage: state.chat.editMessage,
    }),
    shallowEqual
  );

  const [isOwnFetch, setIsOwnFetch] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    return () => {
      dispatch(setMessages([]));
      dispatch(setCanScroll(true));
    };
  }, [dispatch]);

  useEffect(() => {
    if (canScroll) {
      scrollToBottom();
      dispatch(setCanScroll(false));
    }
  }, [dispatch, canScroll]);

  const scrollToBottom = () => {
    bottom.current.scrollIntoView();
  };

  const handleToggleLikeMessage = useCallback(
    (id) => {
      return dispatch(
        toggleLikeMessage({
          userId: ownerId,
          id,
        })
      ).unwrap();
    },
    [dispatch, ownerId]
  );

  const handleDeleteMessage = async (id) => {
    setIsOwnFetch(true);
    try {
      await dispatch(deleteMessage(id)).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsOwnFetch(false);
    }
  };

  const handleSetEditMessage = (id) => {
    dispatch(setEditMessage(id));
  };

  if (editMessage) {
    return <Redirect to="/message-editor" />;
  }

  const messageList = () => {
    const elements = [];
    messages.forEach((message) => {
      if (
        !timeOfLastDivider ||
        moment(message.createdAt)
          .startOf("day")
          .diff(moment(timeOfLastDivider).startOf("day"), "days") !== 0
      ) {
        timeOfLastDivider = message.createdAt;
        elements.push(
          <MessageListDivider
            key={timeOfLastDivider}
            date={timeOfLastDivider}
          />
        );
      }
      if (message.userId === ownerId) {
        elements.push(
          <OwnMessage
            key={message.id}
            id={message.id}
            text={message.text}
            createdAt={message.createdAt}
            setEditMessage={handleSetEditMessage}
            deleteMessage={handleDeleteMessage}
            isFetch={isOwnFetch}
          />
        );
      } else {
        elements.push(
          <Message
            key={message.id}
            id={message.id}
            user={message.user}
            avatar={message.avatar}
            text={message.text}
            createdAt={message.createdAt}
            isLiked={message.likers.includes(ownerId)}
            toggleLikeMessage={handleToggleLikeMessage}
          />
        );
      }
    });

    return elements;
  };

  timeOfLastDivider = null;

  return (
    <div className="message-list">
      {messageList()}
      <div ref={bottom}></div>
    </div>
  );
};

const MessageListDivider = (props) => {
  const divideDate = (date) => {
    const currentDate = moment().startOf("day");
    const formatDate = moment(date).startOf("day");
    const diff = currentDate.diff(formatDate, "days");
    switch (diff) {
      case 0:
        return "Today";
      case 1:
        return "Yesterday";

      default:
        return formatDate.format("dddd, D MMMM");
    }
  };

  return (
    <div className="messages-divider">
      <div className="messages-divider-line"></div>
      <div className="messages-divider-date">{divideDate(props.date)}</div>
    </div>
  );
};

export default MessageList;
