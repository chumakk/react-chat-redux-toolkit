import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { updateMessage, unsetEditMessage } from "../../../store/chat/actions";
import { NotificationManager } from "react-notifications";
import Preloader from "../../Preloaders/middlePreloader/Preloader";
import "./MessageEditor.css";

const MessageEditor = () => {
  const editMessage = useSelector((state) => state.chat.editMessage);
  const dispatch = useDispatch();
  const [input, setInput] = useState(editMessage?.text);

  const [isFetch, setIsFetch] = useState(false);

  useEffect(() => {
    return () => {
      if (editMessage) {
        dispatch(unsetEditMessage());
      }
    };
  }, [dispatch, editMessage]);

  if (!editMessage) {
    return <Redirect to="/chat" />;
  }

  const handleSaveMessage = async () => {
    if (input.length === 0) return;
    const message = {
      id: editMessage.id,
      text: input,
    };
    setIsFetch(true);
    try {
      await dispatch(updateMessage(message)).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  };

  return (
    <div className="message-editor">
      <div className="message-editor-title">Edit message</div>
      <textarea
        value={input}
        onChange={(e) => setInput(e.target.value)}
        className="message-editor-input"
      />
      <div className="message-editor-btn-wrapper">
        <button
          onClick={handleSaveMessage}
          className="message-editor-btn message-editor-save"
        >
          Save
        </button>
        <button
          onClick={() => dispatch(unsetEditMessage())}
          className="message-editor-btn"
        >
          Cancel
        </button>
      </div>
      {isFetch && <Preloader />}
    </div>
  );
};

export default MessageEditor;
