import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { NotificationManager } from "react-notifications";
import { fetchMessages, setEditMessage } from "../../store/chat/actions";
import "./Chat.css";
import Header from "./Header/Header";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import Preloader from "../Preloaders/mainPreloader/Preloader";

const Chat = () => {
  const { ownerId, messages } = useSelector(
    (state) => ({
      ownerId: state.user.user.id,
      messages: state.chat.messages,
    }),
    shallowEqual
  );

  const dispatch = useDispatch();

  const [idFetch, setIsFetch] = useState(false);

  const handleFetchMessages = useCallback(async () => {
    setIsFetch(true);
    try {
      await dispatch(fetchMessages()).unwrap();
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  }, [dispatch]);

  useEffect(() => {
    handleFetchMessages();
  }, [handleFetchMessages]);

  const handleKeyDown = useCallback(
    (e) => {
      if (e.key === "ArrowUp") {
        const lastMessage = [...messages]
          .reverse()
          .find((mes) => mes.userId === ownerId);
        if (lastMessage) {
          e.preventDefault();
          dispatch(setEditMessage(lastMessage.id));
        }
      }
    },
    [dispatch, messages, ownerId]
  );

  useEffect(() => {
    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, [handleKeyDown]);

  return (
    <div className="chat">
      {!idFetch ? (
        <div className="chat-wrapper">
          <Header />
          <MessageList />
          <MessageInput />
        </div>
      ) : (
        <Preloader />
      )}
    </div>
  );
};

export default Chat;
