import React, { useState } from "react";
import { NotificationManager } from "react-notifications";
import { useDispatch, useSelector } from "react-redux";
import { addMessage } from "../../../store/chat/actions";
import Preloader from "../../Preloaders/middlePreloader/Preloader";
import "./MessageInput.css";
import icon from "./paper-plane-solid.svg";

const MessageInput = () => {
  const user = useSelector((state) => state.user.user);

  const dispatch = useDispatch();

  const [textarea, setTextarea] = useState("");
  const [isFetch, setIsFetch] = useState(false);

  const onChangeHandler = (e) => {
    setTextarea(e.target.value);
  };

  const onSendMessage = async () => {
    if (textarea.length === 0) return;
    const message = {
      text: textarea,
      userId: user.id,
      user: user.user,
      avatar: user.avatar,
    };
    setIsFetch(true);
    try {
      await dispatch(addMessage(message)).unwrap();
      setTextarea("");
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  };

  return (
    <div className="message-input">
      <textarea
        className="message-input-text"
        placeholder="Write message..."
        value={textarea}
        onChange={onChangeHandler}
      ></textarea>
      <div className="button-wrapper">
        <button className="message-input-button" onClick={onSendMessage}>
          <img className="sendArrow" src={icon} alt="arrow" />
          Send
        </button>
      </div>
      {isFetch && <Preloader />}
    </div>
  );
};

export default MessageInput;
