import React, { useCallback } from "react";
import moment from "moment";
import { useSelector, shallowEqual } from "react-redux";
import "./Header.css";

const Header = () => {
  const { messages } = useSelector(
    (state) => ({
      messages: state.chat.messages,
    }),
    shallowEqual
  );

  const countOfUsers = useCallback((messages) => {
    const uniqueUsers = [];
    messages.forEach((message) => {
      if (!uniqueUsers.includes(message.userId)) {
        uniqueUsers.push(message.userId);
      }
    });
    return uniqueUsers.length;
  }, []);

  const dateOfLastMessage = useCallback((messages) => {
    const sortedMessages = [...messages].sort((message, nextMessage) => {
      return new Date(message.createdAt) - new Date(nextMessage.createdAt);
    });
    return sortedMessages[sortedMessages.length - 1]?.createdAt;
  }, []);

  const dateOfLastMes = dateOfLastMessage(messages);
  return (
    <div className="header">
      <div className="header-title">My chat</div>
      <div className="header-users-count-wrapper">
        <span className="header-users-count">{countOfUsers(messages)}</span>
        <span> users</span>
      </div>
      <div className="header-messages-count-wrapper">
        <span className="header-messages-count">{messages.length}</span>
        <span> messages</span>
      </div>
      <div className="header-last-message-date-wrapper">
        <span>last message at </span>
        <span className="header-last-message-date">
          {dateOfLastMes && moment(dateOfLastMes).format("DD.MM.YYYY HH:mm")}
        </span>
      </div>
    </div>
  );
};

export default Header;
