import moment from "moment";
import React, { useState } from "react";
import { NotificationManager } from "react-notifications";
import "./Message.css";

const Message = (props) => {
  const [isFetch, setIsFetch] = useState(false);

  const handleToggleLikeMessage = async () => {
    setIsFetch(true);
    try {
      await props.toggleLikeMessage(props.id);
    } catch (e) {
      NotificationManager.error(e.message);
    } finally {
      setIsFetch(false);
    }
  };

  return (
    <div className="chat-message">
      <div className="message-container">
        <div className="message-user-name">{props.user}</div>
        <img
          src={props.avatar}
          className="message-user-avatar"
          alt="avatar"
        ></img>
        <div className="message-info">
          <div className="message-text">{props.text}</div>
          <div className="message-details">
            <span>sended at </span>
            <div className="message-time">
              {moment(props.createdAt).format("HH:mm")}
            </div>
          </div>
        </div>
        <button
          className={props.isLiked ? "message-liked" : "message-like"}
          disabled={isFetch}
          onClick={handleToggleLikeMessage}
        >
          {!isFetch ? (
            <svg
              className="message-icon"
              aria-hidden="true"
              focusable="false"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
            >
              <path
                fill={props.isLiked ? "#e53b3b" : "#888"}
                d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"
              ></path>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              width="20px"
              height="20px"
              viewBox="0 0 100 100"
              preserveAspectRatio="xMidYMid"
            >
              <circle
                cx="50"
                cy="50"
                fill="none"
                stroke="#6d6d6d"
                strokeWidth="8"
                r="24"
                strokeDasharray="113.09733552923255 39.69911184307752"
              >
                <animateTransform
                  attributeName="transform"
                  type="rotate"
                  repeatCount="indefinite"
                  dur="0.8695652173913042s"
                  values="0 50 50;360 50 50"
                  keyTimes="0;1"
                ></animateTransform>
              </circle>
            </svg>
          )}
        </button>
      </div>
    </div>
  );
};

export default Message;
