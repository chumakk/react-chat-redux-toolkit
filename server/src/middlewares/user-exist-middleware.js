import { user as userRepository } from "../repositories";

const isUserExist = (req, res, next) => {
  const user = userRepository.getOne({ user: req.body.user });
  if (user) {
    next({ status: 400, message: "User with this name exist" });
  } else {
    next();
  }
};

export default isUserExist;
