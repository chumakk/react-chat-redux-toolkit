import {
  user as userRepository,
  message as messageRepository,
} from "../repositories";
import User from "./userService";
import Message from "./messageService";
import Auth from "./authService";

const userService = new User(userRepository);

const messageService = new Message(messageRepository);

const authService = new Auth(userRepository);

export { userService, messageService, authService };
