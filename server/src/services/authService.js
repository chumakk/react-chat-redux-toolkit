class Auth {
  constructor(userRepository) {
    this._userRepository = userRepository;
  }

  login(user) {
    return this._userRepository.getOne({user});
  }
}

export default Auth;
