class Message {
  constructor(messageRepository) {
    this._messageRepository = messageRepository;
  }

  getMessages() {
    return this._messageRepository.getAll();
  }

  createMessage(data) {
    const { userId, avatar, user, text } = data;
    const message = { userId, avatar, user, text, likers: [] };
    return this._messageRepository.create(message);
  }

  updateMessage({ id, text }) {
    return this._messageRepository.update(id, { text });
  }

  toggleLikeMessage({ userId, id }) {
    const message = this._messageRepository.getOne({ id });
    const likers = message.likers;
    const newLikers = likers.includes(userId)
      ? likers.filter((id) => id !== userId)
      : [...likers, userId];

    return this._messageRepository.update(id, { likers: newLikers });
  }

  deleteMessage(id) {
    return this._messageRepository.delete(id);
  }
}

export default Message;
