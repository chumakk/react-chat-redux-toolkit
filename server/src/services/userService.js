class User {
  constructor(userRepository) {
    this._userRepository = userRepository;
  }

  getUsers() {
    const users = this._userRepository.getAll();
    return users.map(({ user, avatar, role, id }) => ({
      user,
      avatar,
      role,
      id,
    }));
  }

  getUserById(id) {
    return this._userRepository.getOne({ id });
  }

  createUser(data) {
    const { user, password, avatar = null, role = "user" } = data;
    return this._userRepository.create({ user, password, avatar, role });
  }

  updateUser({ id, ...data }) {
    const { user, password, avatar, role } = data;
    const userData = { user, password, avatar, role };
    for (const [key, value] of Object.entries(userData)) {
      if (value === undefined) {
        delete userData[key];
      }
    }
    return this._userRepository.update(id, userData);
  }

  deleteUser(id) {
    return this._userRepository.delete(id);
  }
}

export default User;
