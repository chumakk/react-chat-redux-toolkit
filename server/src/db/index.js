import { join } from "path";
import low from "lowdb";
import FileSync from "lowdb/adapters/FileSync";

const file = join(__dirname, "db.json");
const adapter = new FileSync(file);
const db = low(adapter);

db.defaults({ messages: [], users: [] }).write();

export default db;
