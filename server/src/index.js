import express, { Router } from "express";
import path from "path";
import fs from 'fs';
import cors from "cors";
import { API_PATH } from "./enums/api-path";
import errorHandler from "./middlewares/error-middleware";
import api from "./api";
import "./db";
const app = express();
const port = 3001;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(API_PATH.API, api(Router));

const staticPath = path.resolve(`${__dirname}/../../client/build`);

app.use(express.static(staticPath));

app.get("*", (_req, res) => {
  res.write(fs.readFileSync(`${staticPath}/index.html`));
  res.end();
});

app.use(errorHandler);

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`);
});
