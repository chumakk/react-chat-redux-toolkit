import initUsers from "./users";
import initMessages from "./message";
import initAuth from "./auth";
import { API_PATH } from "../enums";
import { userService, messageService, authService } from "../services";

const api = (Router) => {
  const apiRouter = Router();

  apiRouter.use(API_PATH.USERS, initUsers(Router, { userService }));

  apiRouter.use(API_PATH.MESSAGES, initMessages(Router, { messageService }));

  apiRouter.use(API_PATH.AUTH, initAuth(Router, { authService }));

  return apiRouter;
};

export default api;
