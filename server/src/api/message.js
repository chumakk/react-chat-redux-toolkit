import { MESSAGES_PATH } from "../enums";

const initMessages = (Router, services) => {
  const { messageService } = services;
  const router = Router();

  router
    .get(MESSAGES_PATH.ROOT, (req, res) => {
      const messages = messageService.getMessages();
      res.json(messages);
    })
    .post(MESSAGES_PATH.ROOT, (req, res) => {
      const message = messageService.createMessage(req.body);
      res.json(message);
    })
    .delete(MESSAGES_PATH.ROOT, (req, res) => {
      const message = messageService.deleteMessage(req.body.id);
      res.json(message);
    })
    .post(MESSAGES_PATH.REACT, (req, res) => {
      const message = messageService.toggleLikeMessage(req.body);
      res.json(message);
    })
    .put(MESSAGES_PATH.UPDATE, (req, res) => {
      const message = messageService.updateMessage(req.body);
      res.json(message);
    });

  return router;
};

export default initMessages;
