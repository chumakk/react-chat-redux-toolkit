import { AUTH_PATH } from "../enums";

const initAuth = (Router, services) => {
  const { authService } = services;
  const router = Router();

  router.post(AUTH_PATH.ROOT, (req, res, next) => {
    const user = authService.login(req.body.user);
    if (!user) {
      next({ status: 400, message: "User doesn't exist" });
      return;
    }

    if (user.password !== req.body.password) {
      next({ status: 400, message: "Password isn't correct" });
      return;
    }

    res.json(user);
  });

  return router;
};

export default initAuth;
