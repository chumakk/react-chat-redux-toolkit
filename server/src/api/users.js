import { USERS_PATH } from "../enums/users-path";
import isUserExist from "../middlewares/user-exist-middleware";

const initUsers = (Router, services) => {
  const { userService } = services;
  const router = Router();

  router
    .get(USERS_PATH.ROOT, (req, res) => {
      const users = userService.getUsers();
      res.json(users);
    })
    .post(USERS_PATH.ROOT, isUserExist, (req, res) => {
      const user = userService.createUser(req.body);
      res.json(user);
    })
    .get(USERS_PATH.ID, (req, res) => {
      const user = userService.getUserById(req.params.id);
      res.json(user);
    })
    .put(USERS_PATH.UPDATE, (req, res) => {
      const user = userService.updateUser(req.body);
      res.json(user);
    })
    .delete(USERS_PATH.ROOT, (req, res) => {
      const user = userService.deleteUser(req.body.id);
      res.json(user);
    });

  return router;
};

export default initUsers;
