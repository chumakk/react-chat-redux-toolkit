const USERS_PATH = {
  ROOT: "/",
  ID: "/:id",
  UPDATE: "/update",
};

export { USERS_PATH };
