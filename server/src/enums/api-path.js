const API_PATH = {
  API: "/api",
  USERS: "/users",
  MESSAGES: "/messages",
  AUTH: "/auth",
};

export { API_PATH };
