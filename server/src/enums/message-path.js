const MESSAGES_PATH = {
  ROOT: "/",
  UPDATE: "/update",
  REACT: "/react",
};

export { MESSAGES_PATH };
