import BaseRepository from "./baseRepository";

class MessageRepository extends BaseRepository{
    constructor(){
        super("messages");
    }
}

export default MessageRepository;