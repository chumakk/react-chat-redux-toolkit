import UserRepository from "./userRepository";
import MessageRepository from "./messageRepository";

const user = new UserRepository();

const message = new MessageRepository();

export { user, message };
